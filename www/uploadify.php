<?php

include __DIR__ . '/../vendor/autoload.php';

$image = Nette\Utils\Image::fromFile($_FILES['Filedata']['tmp_name']);
$path = __DIR__ . '/static/img/udalosti/upload/';

// photo name
$name = Nette\Utils\Strings::webalize(Nette\Utils\Random::generate(15)) . '.jpg';

$image->save($path . '/' . $name, 70);

new \Nette\Http\Response(new \Nette\Application\Responses\JsonResponse(array('asd' => 'asd')));