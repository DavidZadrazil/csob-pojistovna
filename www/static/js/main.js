$(function() {

    /**
     * Photos main page
     */
    $('ul.photos li img').hover(function(){
        $(this).parent().find('.overlay').fadeIn(100);
    });

    $('div.overlay.hidden').mouseleave(function() {
        $(this).fadeOut(100);
    });

    $('div.overlay.no-style').hover(function(){
        $(this).parent().find('img').attr('src', 'static/img/portrait-last-hover.png');
    }, function(){
        $(this).parent().find('img').attr('src', 'static/img/portrait-last.png');
    });


    /**
     * Show text - personal text
     */
    $('#readall').click(function(){
        $('#readall-text').toggle();

        if ($('#readall-text').is(':visible'))
        {
            $('#readall').text('SkrĂ˝t text');
            $('#readall').addClass('active');
        } else {
            $('#readall').text('CelĂ˝ text');
            $('#readall').removeClass('active');
        }
    });

    /**
     * Hierarchy
     */
    $('div.photo.extented').click(function(){

        var clicked = $(this).data('name');

        $(this).parent().find('.active').removeClass('active');
        $(this).parent().parent().find('.active').removeClass('active');
        $(this).addClass('active');

        $('div.sub-person').addClass('hidden');
        $('#' + clicked).removeClass('hidden');
    });

    /*$('div.photo img.portrait.small').hover(function(){
     $(this).parent().find('.overlay').stop(true,false).fadeTo(50, 1);
     });

     $('div.photo .overlay').mouseleave(function() {
     $(this).stop(true,false).fadeTo(50, 0);
     });*/

    $('.tree-small').mouseover(function () {
        $(this).find('.overlay').show();
    }).mouseout(function () {
        $(this).find('.overlay').hide();
    });

    if ($('ul.cities.person').length) {
        recountMiddleLine = function () {

            var levelOneObject = $(".level1 li.active");
            var mainCenter = Math.round(levelOneObject.position().left + levelOneObject.outerWidth() / 2);
            var subObject = $('.level2[data-parent=' + levelOneObject.attr("data-town") + ']');

            var subActiveItem = subObject.find('.active');
            var subActiveCenter = Math.round(subActiveItem.position().left + subActiveItem.outerWidth() / 2);

            var lineLeftPosition = 0;
            if (subActiveCenter > mainCenter) {
                lineLeftPosition = mainCenter;
            } else {
                lineLeftPosition = subActiveCenter;
            }
            var lineWidth = Math.round(subActiveCenter - mainCenter);

            $("#middleLine").css("left", Math.abs(lineLeftPosition) + "px");
            $("#middleLine").css("width", Math.abs(lineWidth) + "px");
        }

        recountTopLine = function () {
            var zeroLevelObject = $(".level0 li.active");
            var firstLevelObject = $(".level1 li.active");
            var mainCenter = Math.round(zeroLevelObject.position().left + zeroLevelObject.outerWidth() / 2);
            var activeCenter = Math.round(firstLevelObject.position().left + firstLevelObject.outerWidth() / 2);
            var lineWidth = mainCenter - activeCenter;

            var lineLeftPos = 0;
            if (activeCenter > mainCenter) {
                lineLeftPos = mainCenter;
            } else {
                lineLeftPos = activeCenter;
            }

            $("#topLine").css("left", Math.abs(lineLeftPos) + "px");
            $("#topLine").css("width", Math.abs(lineWidth) + "px");
        }

        recountMiddleLine();
        //recountTopLine();

        $(".level1 a").on('click', function () {
            $(".level1").find('.active').removeClass('active');
            $(this).parent('li').addClass('active');

            $(".level2.active").removeClass('active');
            $(".level2[data-parent=" + $(this).parent('li').attr("data-town") + "]").addClass('active');

            var clicked = $(this).parent('li').data('town');

            // content
            $('div.town.active').removeClass('active');
            $('#' + clicked).addClass('active');
            $('#' + clicked + '-extended').addClass('active');
            $('#more-' + clicked).addClass('active');

            recountMiddleLine();
            //recountTopLine();
        });

        $(".level2 a").on('click', function () {
            $(this).closest('ul').find('.active').removeClass('active');
            $(this).parent('li').addClass('active');

            var city = $(this).closest('ul').data('parent');
            var clicked = $(this).parent('li').data('street');

            // content
            $('#' + city).find('[data-street]').hide();
            $('#' + city).find('[data-street=' + clicked + ']').show();

            recountMiddleLine();
        });
        $(".level2 a").hover(function () {
            recountMiddleLine();
        });
    }

    if ($('ul.cities').length) {
        $(".level0 a").on('click', function () {
            var clicked = $(this).parent('li').data('target');
            $(this).closest('ul').find('.active').removeClass('active');
            $(this).parent('li').addClass('active');

            if (clicked == "pobocky") {
                $('[data-child=pobocky]').show();
                $('[data-child=reditelstvi]').hide();

                recountTopLine();
                recountMiddleLine();
            } else {
                $('[data-child=pobocky]').hide();
                $('[data-child=reditelstvi]').show();
            }
        });

        $(".level1 a").on('click', function () {
            $(".level1").find('.active').removeClass('active');
            $(this).parent('li').addClass('active');

            $(".level2.active").removeClass('active');
            $(".level2[data-parent=" + $(this).parent('li').attr("data-town") + "]").addClass('active');

            var clicked = $(this).parent('li').data('town');

            // content
            $('[data-child=pobocky] div.town.active').removeClass('active');
            $('#' + clicked).addClass('active');
            $('#' + clicked + '-extended').addClass('active');
            $('#more-' + clicked).addClass('active');

            recountMiddleLine();
            recountTopLine();
        });

        $(".level2 a").on('click', function () {
            $(this).closest('ul').find('.active').removeClass('active');
            $(this).parent('li').addClass('active');

            var city = $(this).closest('ul').data('parent');
            var clicked = $(this).parent('li').data('street');

            // content
            $('#' + city).find('[data-street]').hide();
            $('#' + city).find('[data-street=' + clicked + ']').show();

            recountMiddleLine();
        });
        $(".level2 a").hover(function () {
            recountMiddleLine();
        });

        $(document).ready(function() {
            setTimeout("recountMiddleLine()", 100);
            setTimeout("recountTopLine()", 100);
        });
    }


    recountMiddleLine = function () {

        var levelOneObject = $(".level1 li.active");
        var mainCenter = Math.round(levelOneObject.position().left + levelOneObject.outerWidth() / 2);
        var subObject = $('.level2[data-parent=' + levelOneObject.attr("data-town") + ']');

        var subActiveItem = subObject.find('.active');
        var subActiveCenter = Math.round(subActiveItem.position().left + subActiveItem.outerWidth() / 2);

        var lineLeftPosition = 0;
        if (subActiveCenter > mainCenter) {
            lineLeftPosition = mainCenter;
        } else {
            lineLeftPosition = subActiveCenter;
        }
        var lineWidth = Math.round(subActiveCenter - mainCenter);

        $("#middleLine").css("left", Math.abs(lineLeftPosition) + "px");
        $("#middleLine").css("width", Math.abs(lineWidth) + "px");
    }

    recountTopLine = function () {
        var zeroLevelObject = $(".level0 li.active");
        var firstLevelObject = $(".level1 li.active");
        var mainCenter = Math.round(zeroLevelObject.position().left + zeroLevelObject.outerWidth() / 2);
        var activeCenter = Math.round(firstLevelObject.position().left + firstLevelObject.outerWidth() / 2);
        var lineWidth = mainCenter - activeCenter;

        var lineLeftPos = 0;
        if (activeCenter > mainCenter) {
            lineLeftPos = mainCenter;
        } else {
            lineLeftPos = activeCenter;
        }

        $("#topLine").css("left", Math.abs(lineLeftPos) + "px");
        $("#topLine").css("width", Math.abs(lineWidth) + "px");
    }

});