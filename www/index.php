<?php

// Uncomment this line if you must temporarily take down your site for maintenance.
// require '.maintenance.php';

if(isset($_REQUEST[session_name()])) {
        $_COOKIE[session_name()] = $_REQUEST[session_name()];
}

$container = require __DIR__ . '/../app/bootstrap.php';

$container->getService('application')->run();
