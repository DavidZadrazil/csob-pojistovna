<?php

/**
 * Copyright (c) 2014 David Zadražil (me@davidzadrazil.cz)
 *
 * For the full copyright and license information, please view the file license.txt that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Group
 *
 * @author David Zadražil <me@davidzadrazil.cz>
 *
 * @ORM\Entity
 */
class Office extends BaseEntity
{

        /**
         * @ORM\Column(type="string")
         */
        protected $name;

        /**
         * @ORM\Column(type="string")
         */
        protected $telephone;

        /**
         * @ORM\Column(type="string")
         */
        protected $mobile;

        /**
         * @ORM\Column(type="text")
         */
        protected $address;

        /**
         * @ORM\Column(type="text")
         */
        protected $openTime;

        /**
         * @ORM\ManyToMany(targetEntity="Photo")
         * @ORM\JoinTable(
         *     name="office_photo",
         *     joinColumns={
         *         @ORM\JoinColumn(name="office_id", referencedColumnName="id")
         *     },
         *     inverseJoinColumns={
         *         @ORM\JoinColumn(name="photo_id", referencedColumnName="id")
         *     }
         * )
         */
        protected $photos;

        /**
         * @ORM\ManyToOne(targetEntity="City", inversedBy="offices")
         * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
         */
        protected $city;

        /**
         * @ORM\ManyToMany(targetEntity="Employee", mappedBy="offices")
         * @ORM\OrderBy({"firstname" = "ASC"})
         */
        protected $employees;

        /**
         * @param Photo $photo
         * @return $this
         */
        public function removePhoto(Photo $photo)
        {
                if ($this->photos->contains($photo)) {
                        $this->photos->removeElement($photo);
                }

                return $this;
        }

        /**
         * @param Photo $photo
         * @return $this
         */
        public function addPhoto(Photo $photo)
        {
                if (!$this->photos->contains($photo)) {
                        $this->photos->add($photo);
                }

                return $this;
        }

        /** Constructor */
        public function __construct()
        {
                $this->employees = new ArrayCollection;
                $this->photos = new ArrayCollection;
        }

}