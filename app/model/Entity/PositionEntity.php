<?php

/**
 * Copyright (c) 2014 David Zadražil (me@davidzadrazil.cz)
 *
 * For the full copyright and license information, please view the file license.txt that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Position
 *
 * @author David Zadražil <me@davidzadrazil.cz>
 *
 * @ORM\Entity
 */
class Position extends BaseEntity
{
        /**
         * @ORM\Column(type="string")
         */
        protected $name;

        /**
         * @ORM\OneToMany(targetEntity="Employee", mappedBy="rank")
         * @ORM\OrderBy({"firstname" = "ASC"})
         */
        protected $employees;

        /** Constructor */
        public function __construct()
        {
                $this->employees = new ArrayCollection();
        }
}