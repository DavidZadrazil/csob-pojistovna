<?php

/**
 * Copyright (c) 2014 David Zadražil (me@davidzadrazil.cz)
 *
 * For the full copyright and license information, please view the file license.txt that was distributed with this source code.
 */

namespace App\Entity;

use Kdyby;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class BaseEntity
 *
 * @author David Zadražil <me@davidzadrazil.cz>
 */
abstract class BaseEntity extends Kdyby\Doctrine\Entities\BaseEntity
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

}