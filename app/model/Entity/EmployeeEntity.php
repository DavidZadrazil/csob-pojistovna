<?php

/**
 * Copyright (c) 2014 David Zadražil (me@davidzadrazil.cz)
 *
 * For the full copyright and license information, please view the file license.txt that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Employee
 *
 * @author David Zadražil <me@davidzadrazil.cz>
 *
 * @ORM\Entity
 */
class Employee extends BaseEntity
{

        /**
         * @ORM\Column(type="string", nullable=true)
         */
        protected $firstname;

        /**
         * @ORM\Column(type="string", nullable=true)
         */
        protected $lastname;

        /**
         * @ORM\Column(type="string", nullable=true)
         */
        protected $sestypad;

        /**
         * @ORM\Column(type="boolean")
         */
        protected $hasProfile = TRUE;

        /**
         * @ORM\Column(type="string", nullable=true)
         */
        protected $slug;

        /**
         * @ORM\Column(type="string", nullable=true)
         */
        protected $position;

        /**
         * @ORM\Column(type="string", nullable=true)
         */
        protected $email;

        /**
         * @ORM\Column(type="string", nullable=true)
         */
        protected $mobile;

        /**
         * @ORM\Column(type="text", nullable=true)
         */
        protected $profile;

        /**
         * @ORM\Column(type="text", nullable=true)
         */
        protected $more;

        /**
         * @ORM\Column(type="text", nullable=true)
         */
        protected $bestin;

        /**
         * @ORM\Column(type="text", nullable=true)
         */
        protected $motto;

        /**
         * @ORM\Column(type="text", nullable=true)
         */
        protected $address;

        /**
         * @ORM\Column(type="string", nullable=true)
         */
        protected $photo;

        /**
         * @ORM\Column(type="string", nullable=true)
         */
        protected $signature;

        /**
         * @ORM\ManyToMany(targetEntity="Office");
         */
        protected $offices;

        /**
         * @ORM\ManyToOne(targetEntity="Position")
         * @ORM\JoinColumn(name="position_id", referencedColumnName="id")
         **/
        protected $rank;

        /**
         * @ORM\OneToOne(targetEntity="Employee")
         */
        protected $higher;

        /**
         * @ORM\OneToMany(targetEntity="Employee", mappedBy="higher")
         */
        protected $sub;

        /**
         * @param Office $office
         * @return $this
         */
        public function removeOffice(Office $office)
        {
                if ($this->offices->contains($office)) {
                        $this->offices->removeElement($office);
                }

                return $this;
        }

        /**
         * @param Office $office
         * @return $this
         */
        public function addOffice(Office $office)
        {
                if (!$this->offices->contains($office)) {
                        $this->offices->add($office);
                }

                return $this;
        }

        /** Constructor */
        public function __construct()
        {
                $this->offices = new ArrayCollection;
        }

}