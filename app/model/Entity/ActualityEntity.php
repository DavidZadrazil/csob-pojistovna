<?php

/**
 * Copyright (c) 2014 David Zadražil (me@davidzadrazil.cz)
 *
 * For the full copyright and license information, please view the file license.txt that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Actuality
 *
 * @author David Zadražil <me@davidzadrazil.cz>
 *
 * @ORM\Entity
 */
class Actuality extends BaseEntity
{
        /**
         * @ORM\Column(type="string")
         */
        protected $title;

        /**
         * @ORM\Column(type="string")
         */
        protected $date;

        /**
         * @ORM\Column(type="text")
         */
        protected $perex;

        /**
         * @ORM\Column(type="text")
         */
        protected $text;

        /**
         * @ORM\ManyToMany(targetEntity="Tag", inversedBy="articles")
         * @ORM\JoinTable(
         *     name="actuality_tag",
         *     joinColumns={
         *         @ORM\JoinColumn(name="actuality_id", referencedColumnName="id")
         *     },
         *     inverseJoinColumns={
         *         @ORM\JoinColumn(name="tag_id", referencedColumnName="id")
         *     }
         * )
         */
        protected $tags;

        /**
         * @ORM\ManyToMany(targetEntity="Photo", inversedBy="articles")
         * @ORM\JoinTable(
         *     name="actuality_photo",
         *     joinColumns={
         *         @ORM\JoinColumn(name="actuality_id", referencedColumnName="id")
         *     },
         *     inverseJoinColumns={
         *         @ORM\JoinColumn(name="photo_id", referencedColumnName="id")
         *     }
         * )
         */
        protected $photos;

        /**
         * @param Tag $tag
         * @return $this
         */
        public function removeTag(Tag $tag)
        {
                if ($this->tags->contains($tag)) {
                        $this->tags->removeElement($tag);
                }

                return $this;
        }

        /**
         * @param Tag $tag
         * @return $this
         */
        public function addTag(Tag $tag)
        {
                if (!$this->tags->contains($tag)) {
                        $this->tags->add($tag);
                }

                return $this;
        }

        /**
         * @param Photo $photo
         * @return $this
         */
        public function removePhoto(Photo $photo)
        {
                if ($this->photos->contains($photo)) {
                        $this->photos->removeElement($photo);
                }

                return $this;
        }

        /**
         * @param Photo $photo
         * @return $this
         */
        public function addPhoto(Photo $photo)
        {
                if (!$this->photos->contains($photo)) {
                        $this->photos->add($photo);
                }

                return $this;
        }


        /** Constructor */
        public function __construct()
        {
                $this->tags = new ArrayCollection;
                $this->photos = new ArrayCollection;
        }
}