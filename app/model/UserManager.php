<?php

namespace App\Managers;

use Kdyby\Doctrine\EntityDao;
use Nette;

class UserManager extends Nette\Object implements Nette\Security\IAuthenticator
{

        /** @var \Kdyby\Doctrine\EntityDao */
        private $dao;

        /**
         * @param EntityDao $dao
         */
        public function __construct(EntityDao $dao)
        {
                $this->dao = $dao;
        }

        /**
         * Authenticate user
         *
         * @param array $credentials
         * @return Nette\Security\Identity|Nette\Security\IIdentity
         * @throws Nette\Security\AuthenticationException
         */
        public function authenticate(array $credentials)
        {
                list($email, $password) = $credentials;

                $user = $this->dao->findOneBy(array('email' => $email));

                if (!$user) {
                        throw new Nette\Security\AuthenticationException('E-mail nebo heslo není správné.', self::IDENTITY_NOT_FOUND);
                } elseif (sha1($password) !== $user->password) {
                        throw new Nette\Security\AuthenticationException('E-mail nebo heslo není správné.', self::INVALID_CREDENTIAL);
                }

                return new Nette\Security\Identity($user->id, array('administrator'), array(
                    'email'     => $user->email,
                    'firstname' => $user->firstname,
                    'lastname'  => $user->lastname,
                    'fullname'  => $user->firstname . ' ' . $user->lastname
                ));
        }
}