<?php

namespace App\AdminModule\Presenters;

use Nette;

/**
 * Class HomepagePresenter
 */
class HomepagePresenter extends BasePresenter
{
        /**
         * Render Default
         */
        public function renderDefault()
        {
                $this->redirect('Zamestnanci:default');
        }
}
