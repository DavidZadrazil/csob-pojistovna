<?php

namespace App\AdminModule\Presenters;

use Nette;


/**
 * Class BasePresenter
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{

        protected function startup()
        {
                parent::startup();

                // check if user is logged in
                if (!$this->user->isLoggedIn() && $this->presenter->view !== 'prihlaseni') {
                        $this->redirect('Uzivatel:prihlaseni');
                }
        }

}
