<?php

namespace App\AdminModule\Presenters;

use App\Entity\User;
use Nette;

/**
 * Class UzivatelPresenter
 */
class UzivatelPresenter extends BasePresenter
{

        /** @var \Kdyby\Doctrine\EntityManager @inject */
        public $em;

        /**
         * Logout user
         */
        public function actionOdhlasitSe()
        {
                $this->user->logout(true);
                $this->redirect('Uzivatel:prihlaseni');
        }

        /**
         * @return Nette\Application\UI\Form
         */
        protected function createComponentChangePasswordForm()
        {
                $form = new Nette\Application\UI\Form();

                $form->addPassword('old', 'Staré heslo')
                    ->setRequired('Vyplňte prosím staré heslo.');

                $form->addPassword('new', 'Nové heslo')
                    ->setRequired('Vyplňte prosím nové heslo');

                $form->addSubmit("send", "Změnit heslo");
                $form->onSuccess[] = $this->changePasswordFormSucceeded;

                return $form;
        }

        /**
         * @param Nette\Application\UI\Form $form
         * @param $values
         */
        public function changePasswordFormSucceeded(Nette\Application\UI\Form $form, $values)
        {
                $userDao = $this->em->getDao(User::getClassName());
                $user = $userDao->find($this->user->getId());

                if (sha1($values->old) !== $user->password)
                {
                        $form->addError('Vyplněné staré heslo není správně.');
                } else {
                        $user->password = sha1($values->new);
                        $userDao->save($user);
                }

                $this->flashMessage('Heslo bylo úspěšně upraveno.', 'success');
                $this->redirect('this');
        }

        /**
         * @return Nette\Application\UI\Form
         */
        protected function createComponentSignInForm()
        {
                $form = new Nette\Application\UI\Form();

                $form->addText('email', 'E-mail')
                    ->setRequired('Vyplňte prosím e-mail.');

                $form->addPassword('password', 'Heslo')
                    ->setRequired('Vyplňte prosím heslo.');

                $form->addSubmit("send", "Přihlásit se");
                $form->onSuccess[] = $this->signInFormSucceeded;

                return $form;
        }

        /**
         * @param Nette\Application\UI\Form $form
         * @param $values
         */
        public function signInFormSucceeded(Nette\Application\UI\Form $form, $values)
        {
                $this->getUser()->setExpiration('14 days', false);

                try {
                        $this->getUser()->login($values->email, $values->password);
                        $this->redirect('Zamestnanci:');

                } catch (Nette\Security\AuthenticationException $e) {
                        $form->addError($e->getMessage());
                }
        }

}
