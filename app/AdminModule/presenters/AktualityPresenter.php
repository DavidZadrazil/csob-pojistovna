<?php

namespace App\AdminModule\Presenters;

use App\Entity\Actuality;
use App\Entity\Photo;
use App\Entity\Tag;
use Nette;

/**
 * Class AktualityPresenter
 */
class AktualityPresenter extends BasePresenter
{

        /** @var \Kdyby\Doctrine\EntityManager @inject */
        public $em;

        /**
         * Render Default
         */
        public function renderDefault()
        {
                $actualityDao = $this->em->getDao(Actuality::getClassName());

                $this->template->actualities = $actualityDao->findAll();
        }

        /**
         * @param $id
         */
        public function renderUpravit($id)
        {
                $actualityDao = $this->em->getDao(Actuality::getClassName());

                $this->template->actuality = $actuality = $actualityDao->find($id);

                $this["updateActuality"]->setDefaults(array(
                    "id" => $actuality->id,
                    "title" => $actuality->title,
                    "perex" => $actuality->perex,
                    "date" => $actuality->date,
                    "text" => $actuality->text,
                ));
        }

        /**
         * @param $actuality
         * @param $tag
         */
        public function handleOdstranitTag($actuality, $tag)
        {
                $tagDao = $this->em->getDao(Tag::getClassName());
                $actualityDao = $this->em->getDao(Actuality::getClassName());

                $actuality = $actualityDao->find($actuality);
                $tag = $tagDao->find($tag);

                $actuality->removeTag($tag);

                $actualityDao->save($actuality);

                $this->flashMessage("Tag byl odstraněn.");
                $this->redirect("this");
        }

        /**
         * @param $actuality
         * @param $photo
         */
        public function handleOdstranitFotografii($actuality, $photo)
        {
                $photoDao = $this->em->getDao(Photo::getClassName());
                $actualityDao = $this->em->getDao(Actuality::getClassName());

                $actuality = $actualityDao->find($actuality);
                $photo = $photoDao->find($photo);

                $actuality->removePhoto($photo);

                $actualityDao->save($actuality);

                $this->flashMessage("Fotografie byla odstraněna.");
                $this->redirect("this");
        }

        /**
         * @return Nette\Application\UI\Form
         */
        protected function createComponentUpdateActuality()
        {
                $form = new Nette\Application\UI\Form();

                $form->addHidden("id");

                $form->addText('title', 'Titulek')
                    ->setRequired('Vyplňte prosím titulek.');

                $form->addText('tags', 'Tagy')
                    ->setAttribute('data-role', 'tagsinput');

                $form->addText('date', 'Datum')
                    ->setRequired('Vyplňte prosím datum.');

                $form->addTextArea('perex', 'Perex');

                $form->addTextArea('text', 'Text');

                for ($i = 1; $i <= 25; $i++) {
                        $form->addUpload('photo' . $i);
                }

                $form->addSubmit("send", "Upravit aktualitu");
                $form->onSuccess[] = $this->updateActualitySucceeded;

                return $form;
        }

        /**
         * @param Nette\Application\UI\Form $form
         * @param $values
         */
        public function updateActualitySucceeded(Nette\Application\UI\Form $form, $values)
        {
                $arrayValues = $form->getValues(TRUE);

                $actualityDao = $this->em->getDao(Actuality::getClassName());
                $photoDao = $this->em->getDao(Photo::getClassName());
                $tagDao = $this->em->getDao(Tag::getClassName());

                $actuality = $actualityDao->find($values->id);
                $actuality->title = $values->title;
                $actuality->date = $values->date;
                $actuality->perex = $values->perex;
                $actuality->text = $values->text;

                $tags = explode(",", $values->tags);
                foreach ($tags as $tag) {
                        if (!$tagDao->findBy(array("name" => $tag)) && $tag) {
                                $createTag = new Tag();
                                $createTag->name = $tag;

                                $tagDao->save($createTag);
                        }
                }

                /**
                 * Image process
                 */
                $photos = array();
                for ($i = 1; $i <= 25; $i++) {
                        $name = $arrayValues["photo" . $i];

                        if ($name->isOk()) {
                                $image = Nette\Utils\Image::fromFile($arrayValues["photo" . $i]);
                                $path = __DIR__ . '/../../../www/static/img/udalosti/upload/';

                                // photo name
                                $name = Nette\Utils\Random::generate(15) . '.jpg';

                                $image->save($path . '/' . $name, 80);

                                $photo = new Photo();
                                $photo->name = $name;

                                if ($i == 1) {
                                        $image = Nette\Utils\Image::fromFile($arrayValues["photo" . $i]);
                                        $image->resize(144, 131, Nette\Utils\Image::FILL);
                                        $image->save($path . '/thumb_' . $name, 80);

                                        $photo->thumbnail = TRUE;
                                }

                                $photoDao->save($photo);
                                $photos[] = $name;
                        }
                }

                $actualityDao->save($actuality);

                foreach ($tags as $tag) {
                        if ($tag) $actuality->addTag($tagDao->findOneBy(array("name" => $tag)));
                }

                foreach ($photos as $photo) {
                        $actuality->addPhoto($photoDao->findOneBy(array("name" => $photo)));
                }

                $actualityDao->save($actuality);

                $this->flashMessage("Aktualita byla upravena.");
                $this->redirect("this");
        }

        /**
         * @return Nette\Application\UI\Form
         */
        protected function createComponentCreateActuality()
        {
                $form = new Nette\Application\UI\Form();

                $form->addText('title', 'Titulek')
                    ->setRequired('Vyplňte prosím titulek.');

                $form->addText('tags', 'Tagy')
                    ->setAttribute('data-role', 'tagsinput');

                $form->addText('date', 'Datum')
                    ->setRequired('Vyplňte prosím datum.');

                $form->addTextArea('perex', 'Perex');

                $form->addTextArea('text', 'Text');

                for ($i = 1; $i <= 25; $i++) {
                        $form->addUpload('photo' . $i);
                }

                $form->addSubmit("send", "Vytvořit aktualitu");
                $form->onSuccess[] = $this->createActualitySucceeded;

                return $form;
        }

        /**
         * @param Nette\Application\UI\Form $form
         * @param $values
         */
        public function createActualitySucceeded(Nette\Application\UI\Form $form, $values)
        {
                $arrayValues = $form->getValues(TRUE);

                $actualityDao = $this->em->getDao(Actuality::getClassName());
                $photoDao = $this->em->getDao(Photo::getClassName());
                $tagDao = $this->em->getDao(Tag::getClassName());

                $actuality = new Actuality();
                $actuality->title = $values->title;
                $actuality->date = $values->date;
                $actuality->perex = $values->perex;
                $actuality->text = $values->text;

                $tags = explode(",", $values->tags);
                foreach ($tags as $tag) {
                        if (!$tagDao->findBy(array("name" => $tag)) && $tag) {
                                $createTag = new Tag();
                                $createTag->name = $tag;

                                $tagDao->save($createTag);
                        }
                }

                /**
                 * Image process
                 */
                $photos = array();
                for ($i = 1; $i <= 25; $i++) {
                        $name = $arrayValues["photo" . $i];

                        if ($name->isOk()) {
                                $image = Nette\Utils\Image::fromFile($arrayValues["photo" . $i]);
                                $path = __DIR__ . '/../../../www/static/img/udalosti/upload/';

                                // photo name
                                $name = Nette\Utils\Random::generate(15) . '.jpg';

                                $image->save($path . '/' . $name, 80);

                                $photo = new Photo();
                                $photo->name = $name;

                                if ($i == 1) {
                                        $image = Nette\Utils\Image::fromFile($arrayValues["photo" . $i]);
                                        $image->resize(144, 131, Nette\Utils\Image::FILL | Nette\Utils\Image::EXACT);
                                        $image->save($path . '/thumb_' . $name, 80);

                                        $photo->thumbnail = TRUE;
                                }

                                $photoDao->save($photo);
                                $photos[] = $name;
                        }
                }

                $actualityDao->save($actuality);

                foreach ($tags as $tag) {
                        if ($tag) $actuality->addTag($tagDao->findOneBy(array("name" => $tag)));
                }

                foreach ($photos as $photo) {
                        $actuality->addPhoto($photoDao->findOneBy(array("name" => $photo)));
                }

                $actualityDao->save($actuality);

                $this->flashMessage("Aktualita byla uložena.");
                $this->redirect("this");
        }

        /**
         * @param $id
         */
        public function handleOdstranit($id)
        {
                $actualityDao = $this->em->getDao(Actuality::getClassName());
                $actuality = $actualityDao->find($id);
                $actualityDao->delete($actuality);

                $this->flashMessage("Aktualita byla odstraněna.");
                $this->redirect("Aktuality:default");
        }
}
