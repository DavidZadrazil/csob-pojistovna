<?php

namespace App\AdminModule\Presenters;

use App\Entity\Employee;
use App\Entity\Office;
use App\Entity\Position;
use Nette;

/**
 * Class ZamestnanciPresenter
 */
class ZamestnanciPresenter extends BasePresenter
{

        /** @var \Kdyby\Doctrine\EntityManager @inject */
        public $em;

        /**
         * Render Default
         */
        public function renderDefault()
        {
                $employeesDao = $this->em->getDao(Employee::getClassName());
                $this->template->employees = $employeesDao->findBy(array(), array('lastname' => 'ASC'));
        }

        /**
         * @param $id
         */
        public function handleOdstranit($id)
        {
                $employeesDao = $this->em->getDao(Employee::getClassName());
                $employee = $employeesDao->find($id);

                $employeesDao->delete($employee);

                $this->flashMessage("Zaměstnanec byl odstraněn.", "success");
                $this->redirect("this");
        }

        /**
         * Render Editovat
         */
        public function renderEditovat($id)
        {
                $employeesDao = $this->em->getDao(Employee::getClassName());
                $employee = $employeesDao->find($id);

                if (!$employee) {
                        $this->redirect('Zamestnanci:default');
                } else {
                        $this->template->employee = $employee;

                        $employeeOffices = array();
                        foreach ($employee->offices as $employeeOffice) {
                                $employeeOffices[] = $employeeOffice->id;
                        }

                        $this['userEditForm']->setDefaults(array(
                            'hasProfile' => $employee->hasProfile,
                            'firstname'  => $employee->firstname,
                            'lastname'   => $employee->lastname,
                            'rank'       => $employee->rank->id,
                            'email'      => $employee->email,
                            'mobile'     => $employee->mobile,
                            'profile'    => $employee->profile,
                            'more'       => $employee->more,
                            'motto'      => $employee->motto,
                            'bestin'     => $employee->bestin,
                            'address'    => $employee->address,
                            'higher'     => isset($employee->higher->id) ? $employee->higher->id : 0,
                            'offices'    => $employeeOffices
                        ));
                }
        }

        /**
         * @return Nette\Application\UI\Form
         */
        protected function createComponentUserSignatureForm()
        {
                $form = new Nette\Application\UI\Form();

                $form->addUpload('signature', 'Podpis');

                $form->addSubmit("send", "Nahrát podpis");
                $form->onSuccess[] = $this->userSignatureFormSucceeded;

                return $form;
        }

        /**
         * @param Nette\Application\UI\Form $form
         * @param $values
         */
        public function userSignatureFormSucceeded(Nette\Application\UI\Form $form, $values)
        {
                // employee dao
                $employees = $this->em->getDao(Employee::getClassName());

                $employee = $employees->find($this->getParameter('id'));

                if ($values->signature->isOk()) {
                        $image = Nette\Utils\Image::fromFile($values->signature);
                        $path = __DIR__ . '/../../../www/static/img/signatures/';

                        // photo name
                        $name = Nette\Utils\Strings::webalize($employee->firstname) . '-' . Nette\Utils\Strings::webalize($employee->lastname) . '.jpg';

                        $image->save($path . '/' . $name, 80);

                        $employee->signature = $name;
                        $employees->save($employee);

                } else {
                        $form['signature']->addError('Fotografii se nepodařilo nahrát. (' . $values->signature->getError() . ')');
                }
        }

        /**
         * @return Nette\Application\UI\Form
         */
        protected function createComponentUserPhotoForm()
        {
                $form = new Nette\Application\UI\Form();

                $form->addUpload('photo', 'Fotografie');

                $form->addSubmit("send", "Nahrát fotografii");
                $form->onSuccess[] = $this->userPhotoFormSucceeded;

                return $form;
        }

        /**
         * @param Nette\Application\UI\Form $form
         * @param $values
         */
        public function userPhotoFormSucceeded(Nette\Application\UI\Form $form, $values)
        {
                // employee dao
                $employees = $this->em->getDao(Employee::getClassName());

                $employee = $employees->find($this->getParameter('id'));

                if ($values->photo->isOk()) {
                        $image = Nette\Utils\Image::fromFile($values->photo);
                        $mainPhoto = Nette\Utils\Image::fromFile($values->photo);
                        $path = __DIR__ . '/../../../www/static/img/photos/';

                        // photo name
                        $name = Nette\Utils\Strings::webalize($employee->firstname) . '-' . Nette\Utils\Strings::webalize($employee->lastname) . '.jpg';

                        $percentage = ($image->height * 0.2);

                        // thumbnail
                        $mainPhoto->crop(0, 0, $image->width, $image->height - $percentage);
                        $mainPhoto->resize(392, 369, Nette\Utils\Image::EXACT);
                        $mainPhoto->save($path . 'thumbnail/' . $name, 40);

                        $image->save($path . 'original/' . $name, 30);

                } else {
                        $form['photo']->addError('Fotografii se nepodařilo nahrát. (' . $values->photo->getError() . ')');
                }
        }

        /**
         * @return Nette\Application\UI\Form
         */
        protected function createComponentUserEditForm()
        {
                $form = new Nette\Application\UI\Form();

                $form->addCheckbox('hasProfile');

                $form->addText('firstname', 'Jméno')
                    ->setRequired('Vyplňte prosím jméno.');

                $form->addText('lastname', 'Příjmení')
                    ->setRequired('Vyplňte prosím příjmení.');

                $form->addUpload('photo', 'Fotografie');

                $form->addUpload('signature', 'Podpis');

                // get all highers
                $employeesDao = $this->em->getDao(Employee::getClassName());
                $highers = array();
                $highers[0] = '-- bez nadřízeného --';
                foreach ($employeesDao->findAll() as $higher) {
                        $highers[$higher->id] = $higher->firstname . ' ' . $higher->lastname . ', ' . $higher->rank->name;
                }
                $form->addSelect('higher', 'Nadřízený', $highers);

                // get all positions
                $positionDao = $this->em->getDao(Position::getClassName());
                $positions = array();
                foreach ($positionDao->findAll() as $position) {
                        $positions[$position->id] = $position->name;
                }
                $form->addRadioList('rank', 'Pozice', $positions);

                $form->addText('email', 'E-mail');

                $form->addText('mobile', 'Telefon');

                $form->addTextArea('profile', 'Profil');
                $form->addTextArea('more', 'Více o zaměstnanci');
                $form->addTextArea('motto', 'Motto');
                $form->addTextArea('bestin', 'V čem jsem nejlepší?');
                $form->addTextArea('address', 'Adresa');

                $officesDao = $this->em->getDao(Office::getClassName());
                $offices = $officesDao->findAll();

                $preparedOffices = array();
                foreach ($offices as $office) {
                        $preparedOffices[$office->id] = $office->name;
                }

                $form->addCheckboxList('offices', 'Pracoviště', $preparedOffices);

                $form->addSubmit("send", "Uložit změny");
                $form->onSuccess[] = $this->userEditFormSucceeded;

                return $form;
        }

        /**
         * @param Nette\Application\UI\Form $form
         * @param $values
         */
        public function userEditFormSucceeded(Nette\Application\UI\Form $form, $values)
        {
                $employeesDao = $this->em->getDao(Employee::getClassName());
                $officesDao = $this->em->getDao(Office::getClassName());
                $positionsDao = $this->em->getDao(Position::getClassName());
                $employee = $employeesDao->find($this->getParameter('id'));

                foreach ($employee->offices as $employeeOffice) {
                        $employee->removeOffice($employeeOffice);
                }

                $offices = $values->offices;
                foreach ($offices as $office) {
                        $office = $officesDao->find($office);
                        $employee->addOffice($office);
                }

                /**
                 * Photo process
                 */
                if ($values->photo)
                {
                        if ($values->photo->isOk()) {
                                $image = Nette\Utils\Image::fromFile($values->photo);
                                $mainPhoto = Nette\Utils\Image::fromFile($values->photo);
                                $path = __DIR__ . '/../../../www/static/img/photos/';

                                // photo name
                                $name = Nette\Utils\Strings::webalize($employee->firstname) . '-' . Nette\Utils\Strings::webalize($employee->lastname) . '.jpg';

                                $percentage = ($image->height * 0.2);

                                // thumbnail
                                $mainPhoto->crop(0, 0, $image->width, $image->height - $percentage);
                                $mainPhoto->resize(392, 369, Nette\Utils\Image::EXACT);
                                $mainPhoto->save($path . 'thumbnail/' . $name, 40);

                                $image->save($path . 'original/' . $name, 30);

                                $employee->photo = $name;
                        } else {
                                $form['photo']->addError('Fotografii se nepodařilo nahrát. (' . $values->photo->getError() . ')');
                        }
                }

                /**
                 * Signature process
                 */
                if ($values->signature)
                {
                        if ($values->signature->isOk()) {
                                $image = Nette\Utils\Image::fromFile($values->signature);
                                $path = __DIR__ . '/../../../www/static/img/signatures/';

                                // photo name
                                $name = Nette\Utils\Strings::webalize($employee->firstname) . '-' . Nette\Utils\Strings::webalize($employee->lastname) . '.jpg';

                                $image->save($path . '/' . $name, 80);

                                $employee->signature = $name;
                        } else {
                                $form['signature']->addError('Fotografii se nepodařilo nahrát. (' . $values->signature->getError() . ')');
                        }
                }

                $employee->firstname = $values->firstname;
                $employee->lastname = $values->lastname;
                $employee->rank = $positionsDao->find($values->rank);
                $employee->email = $values->email;
                $employee->mobile = $values->mobile;
                $employee->profile = $values->profile;
                $employee->more = $values->more;
                $employee->motto = $values->motto;
                $employee->bestin = $values->bestin;
                $employee->address = $values->address;
                $employee->hasProfile = $values->hasProfile;
                $employee->higher = ($values->higher == 0) ? NULL : $employeesDao->find($values->higher);

                $employeesDao->save($employee);

                $this->flashMessage('Provedené změny byly uloženy.', 'success');
                //$this->redirect('this');
        }

        /**
         * @return Nette\Application\UI\Form
         */
        protected function createComponentUserCreateForm()
        {
                $form = new Nette\Application\UI\Form();

                $form->addCheckbox('hasProfile');

                $form->addText('firstname', 'Jméno')
                    ->setRequired('Vyplňte prosím jméno.');

                $form->addText('sestypad', 'Šestý pád')
                    ->setRequired('Vyplňte prosím 6. pád křestního jména.');

                $form->addText('lastname', 'Příjmení')
                    ->setRequired('Vyplňte prosím příjmení.');

                // get all positions
                $positionDao = $this->em->getDao(Position::getClassName());
                $positions = array();
                foreach ($positionDao->findAll() as $position) {
                        $positions[$position->id] = $position->name;
                }
                $form->addRadioList('rank', 'Pozice', $positions);

                // get all highers
                $employeesDao = $this->em->getDao(Employee::getClassName());
                $highers = array();
                $highers[0] = '-- bez nadřízeného --';
                foreach ($employeesDao->findAll() as $higher) {
                        $highers[$higher->id] = $higher->firstname . ' ' . $higher->lastname . ', ' . $higher->rank->name;
                }
                $form->addSelect('higher', 'Nadřízený', $highers);

                $form->addText('email', 'E-mail')
                    ->addConditionOn($form['hasProfile'], Nette\Forms\Form::EQUAL, false)
                    ->addRule(Nette\Application\UI\Form::EMAIL, 'Vyplňte prosím platný e-mail.')
                    ->setRequired('Vyplňte prosím e-mail.');

                $form->addText('mobile', 'Telefon');

                $form->addTextArea('profile', 'Profil');
                $form->addTextArea('more', 'Více o zaměstnanci');
                $form->addTextArea('motto', 'Motto');
                $form->addTextArea('bestin', 'V čem jsem nejlepší?');
                $form->addTextArea('address', 'Adresa');

                $form->addUpload('photo');
                $form->addUpload('signature');

                $officesDao = $this->em->getDao(Office::getClassName());
                $offices = $officesDao->findAll();

                $preparedOffices = array();
                foreach ($offices as $office) {
                        $preparedOffices[$office->id] = $office->name;
                }

                $form->addCheckboxList('offices', 'Pracoviště', $preparedOffices);

                $form->addSubmit("send", "Vytvořit zaměstnance");
                $form->onSuccess[] = $this->userCreateFormSucceeded;

                return $form;
        }

        /**
         * @param Nette\Application\UI\Form $form
         * @param $values
         */
        public function userCreateFormSucceeded(Nette\Application\UI\Form $form, $values)
        {
                $employeesDao = $this->em->getDao(Employee::getClassName());
                $positionsDao = $this->em->getDao(Position::getClassName());

                // create clear entity
                $employee = new Employee();

                // process signature image
                if ($values->signature) {
                        if ($values->signature->isOk()) {
                                $image = Nette\Utils\Image::fromFile($values->signature);
                                $path = __DIR__ . '/../../../www/static/img/signatures/';

                                // photo name
                                $name = Nette\Utils\Strings::webalize($values->firstname) . '-' . Nette\Utils\Strings::webalize($values->lastname) . '.jpg';

                                $image->save($path . '/' . $name, 80);

                                $employee->signature = $name;

                        } else {
                                $form->addError('Podpis se nepodařilo nahrát. (' . $values->signature->getError() . ')');
                        }
                }

                // process photo
                if ($values->photo) {
                        if ($values->photo->isOk()) {
                                $image = Nette\Utils\Image::fromFile($values->photo);
                                $mainPhoto = Nette\Utils\Image::fromFile($values->photo);
                                $path = __DIR__ . '/../../../www/static/img/photos/';

                                // photo name
                                $name = Nette\Utils\Strings::webalize($values->firstname) . '-' . Nette\Utils\Strings::webalize($values->lastname) . '.jpg';

                                $percentage = ($image->height * 0.2);

                                // thumbnail
                                $mainPhoto->crop(0, 0, $image->width, $image->height - $percentage);
                                $mainPhoto->resize(392, 369, Nette\Utils\Image::EXACT);
                                $mainPhoto->save($path . 'thumbnail/' . $name, 40);

                                $image->save($path . 'original/' . $name, 30);

                                $employee->photo = $name;

                        } else {
                                $form->addError('Fotografii se nepodařilo nahrát. (' . $values->photo->getError() . ')');
                        }
                }

                $employee->firstname = $values->firstname;
                $employee->lastname = $values->lastname;
                $employee->hasProfile = ($values->hasProfile) ? false : true;
                $employee->rank = $positionsDao->find($values->rank);
                $employee->email = $values->email;
                $employee->mobile = $values->mobile;
                $employee->profile = $values->profile;
                $employee->bestin = $values->bestin;
                $employee->more = $values->more;
                $employee->motto = $values->motto;
                $employee->address = $values->address;
                $employee->sestypad = $values->sestypad;
                $employee->higher = ($values->higher == 0) ? NULL : $employeesDao->find($values->higher);
                $employee->slug = Nette\Utils\Strings::webalize($values->firstname) . '-' . Nette\Utils\Strings::webalize($values->lastname);

                $employeesDao->save($employee);

                $officesDao = $this->em->getDao(Office::getClassName());
                foreach ($values->offices as $office) {
                        $office = $officesDao->find($office);
                        $employee->addOffice($office);
                }

                $employeesDao->save($employee);

                $this->flashMessage('Zaměstnanec byl vytvořen.', 'success');
                $this->redirect('this');
        }


        public function actionPriradit()
        {
                $employeesDao = $this->em->getDao(Employee::getClassName());
                $positionDao = $this->em->getDao(Position::getClassName());

                foreach ($employeesDao->findAll() as $employee) {
                        $employee->hasProfile = true;
                        $employeesDao->save($employee);
                }

                $this->terminate();
        }

}
