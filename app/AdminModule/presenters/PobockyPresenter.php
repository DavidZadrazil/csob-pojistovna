<?php

namespace App\AdminModule\Presenters;

use App;
use App\Entity\City;
use App\Forms\ICityForm;
use Nette;
use Kdyby;

/**
 * Class PobockyPresenter
 */
class PobockyPresenter extends BasePresenter
{

        /** @var Kdyby\Doctrine\EntityManager @inject */
        public $em;

        /**
         * @var App\Forms\ICityForm @inject
         */
        public $cityForm;

        /**
         * @var App\Forms\IOfficeForm @inject
         */
        public $officeForm;

        /**
         * Render Default
         */
        public function renderDefault()
        {
                $cityDao = $this->em->getDao(City::getClassName());

                $this->template->cities = $cityDao->findAll();
        }

        /**
         * @param $id
         */
        public function renderUpravitPobocku($id)
        {
                $officeDao = $this->em->getDao(App\Entity\Office::getClassName());
                $office = $officeDao->find($id);

                $this['updateOfficeForm']->setDefaults(array(
                    'id' => $office->id,
                    'city' => $office->city->id,
                    'name' => $office->name,
                    'telephone' => $office->telephone,
                    'mobile' => $office->mobile,
                    'address' => $office->address,
                    'openTime' => $office->openTime,
                ));

                $this->template->office = $office;
        }

        /**
         * @param $id
         */
        public function renderPridatPobocku($id)
        {
                $this['officeForm']->setDefaults(array(
                    'city' => $id
                ));
        }

        /**
         * @param $id
         */
        public function renderUpravitMesto($id)
        {
                $cityDao = $this->em->getDao(City::getClassName());
                $city = $cityDao->find($id);

                $this['updateCityForm']->setDefaults(array(
                    'name' => $city->name,
                    'id' => $city->id
                ));
        }

        /**
         * @param $id
         */
        public function handleOdstranitMesto($id)
        {
                $cityDao = $this->em->getDao(City::getClassName());
                $city = $cityDao->find($id);
                $cityDao->delete($city);

                $this->flashMessage('Město bylo odstraněno.');
                $this->redirect('this');
        }

        /**
         * @param $office
         * @param $photo
         * @internal param $actuality
         */
        public function handleOdstranitFotografii($office, $photo)
        {
                $photoDao = $this->em->getDao(App\Entity\Photo::getClassName());
                $officeDao = $this->em->getDao(App\Entity\Office::getClassName());

                $office = $officeDao->find($office);
                $photo = $photoDao->find($photo);

                $office->removePhoto($photo);

                $officeDao->save($office);

                $this->flashMessage("Fotografie byla odstraněna.");
                $this->redirect("this");
        }

        /**
         * @param $id
         */
        public function handleOdstranitPobocku($id)
        {
                $officeDao = $this->em->getDao(App\Entity\Office::getClassName());
                $city = $officeDao->find($id);
                $officeDao->delete($city);

                $this->flashMessage('Pobočka byla odstraněna.');
                $this->redirect('this');
        }

        /**
         * @return App\Forms\OfficeForm
         */
        protected function createComponentOfficeForm()
        {
                $control = $this->officeForm->create();

                $control['form']->onSuccess[] = function (Nette\Application\UI\Form $form) {
                        if (!$form->isValid()) return;

                        $this->flashMessage('Město bylo úspěšně přidáno.', 'success');
                        $this->redirect('this');
                };

                return $control;
        }

        /**
         * @return App\Forms\OfficeForm
         */
        protected function createComponentUpdateOfficeForm()
        {
                $control = $this->officeForm->create();

                $control['form']->addHidden('id');
                $control['form']['send']->caption = 'Upravit pobočku';

                $control['form']->onSuccess[] = function (Nette\Application\UI\Form $form) {
                        if (!$form->isValid()) return;

                        $this->flashMessage('Pobočka byla upravena.', 'success');
                        $this->redirect('this');
                };

                return $control;
        }

        /**
         * @return ICityForm
         */
        protected function createComponentCityForm()
        {
                $control = $this->cityForm->create();

                $control['form']->onSuccess[] = function (Nette\Application\UI\Form $form) {
                        if (!$form->isValid()) return;

                        $this->flashMessage('Město bylo úspěšně přidáno.', 'success');
                        $this->redirect('this');
                };

                return $control;
        }

        /**
         * @return ICityForm
         */
        protected function createComponentUpdateCityForm()
        {
                $control = $this->cityForm->create();

                $control['form']->addHidden('id');
                $control['form']['send']->caption = 'Upravit město';

                $control['form']->onSuccess[] = function (Nette\Application\UI\Form $form) {
                        if (!$form->isValid()) return;

                        $this->flashMessage('Město bylo úspěšně upraveno.', 'success');
                        $this->redirect('this');
                };

                return $control;
        }
}
