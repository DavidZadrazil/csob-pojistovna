<?php

namespace App;

use Nette,
    Nette\Application\Routers\RouteList,
    Nette\Application\Routers\Route,
    Nette\Application\Routers\SimpleRouter;


/**
 * Router factory.
 */
class RouterFactory
{

        /**
         * @return \Nette\Application\IRouter
         */
        public function createRouter()
        {
                $router = new RouteList();

                /**
                 * Backend module
                 */
                $router[] = new Route('administrace/<presenter>/<action>[/<id>]', array(
                    'module'    => 'Admin',
                    'presenter' => 'Homepage',
                    'action'    => 'default'
                ));

                /**
                 * Frontend routes
                 */
                $router[] = new Route('cizinci', 'Front:Cizinci:default');
                $router[] = new Route('kontakt', 'Front:Kontakt:default');
                $router[] = new Route('produkty', 'Front:Produkty:default');
                $router[] = new Route('o-nas', 'Front:ONas:default');
                $router[] = new Route('proc-my', 'Front:ProcMy:default');
                $router[] = new Route('kariera', 'Front:Kariera:default');
                $router[] = new Route('<name>', 'Front:Profil:default');
                $router[] = new Route('<presenter>/<action>[/<id>]', 'Front:Homepage:default');
                return $router;
        }

}
