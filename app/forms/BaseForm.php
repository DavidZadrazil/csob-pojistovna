<?php

namespace App\Forms;

use Nette\Application\UI\Control;
use Nette\Forms\Controls\Button;
use Nette\Forms\Controls\Checkbox;
use Nette\Forms\Controls\CheckboxList;
use Nette\Forms\Controls\MultiSelectBox;
use Nette\Forms\Controls\RadioList;
use Nette\Forms\Controls\SelectBox;
use Nette\Forms\Controls\TextBase;
use Nette\Reflection\ClassType;

/**
 * Base form
 *
 * Class RegistrationForm
 */
class BaseForm extends Control
{

        /**
         * Render form
         */
        public function render()
        {
                /**
                 * Custom rendering for Bootstrap 3
                 */
                $formRenderer = $this['form']->getRenderer();
                $formRenderer->wrappers['control']['container'] = null;
                $formRenderer->wrappers['control']['description'] = 'span class=help-block';
                $formRenderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
                $formRenderer->wrappers['controls']['container'] = null;
                $formRenderer->wrappers['pair']['container'] = 'div class=form-group';
                $formRenderer->wrappers['pair']['.error'] = 'has-error';
                $formRenderer->wrappers['label']['container'] = 'div class=control-label';

                foreach ($this['form']->getControls() as $control) {
                        if ($control instanceof Button) {
                                $control->getControlPrototype()->addClass('btn');

                        } elseif ($control instanceof TextBase || $control instanceof SelectBox || $control instanceof MultiSelectBox) {
                                $control->getControlPrototype()->addClass('form-control');

                        } elseif ($control instanceof Checkbox || $control instanceof CheckboxList || $control instanceof RadioList) {
                                $control->getSeparatorPrototype()->setName('div')->addClass($control->getControlPrototype()->type);
                        }
                }

                // template name
                $calledFormName = ClassType::from($this)->getShortName();
                $templatePath = __DIR__ . '/' . $calledFormName . '/' . $calledFormName . '.latte';

                $this->template->setFile($templatePath);
                $this->template->render();
        }

        /**
         * Set default values
         *
         * @param array $values
         */
        public function setDefaults(array $values)
        {
                $this['form']->setDefaults($values);
        }

}