<?php

namespace App\Forms;

use App\Entity\City;
use App\Entity\Office;
use App\Entity\Photo;
use Kdyby;
use Nette;

/**
 * Class OfficeForm
 * @package App\Forms
 */
class OfficeForm extends BaseForm
{
        /** @var Kdyby\Doctrine\EntityManager */
        private $em;

        /**
         * @param Kdyby\Doctrine\EntityManager $em
         */
        public function __construct(Kdyby\Doctrine\EntityManager $em)
        {
                $this->em = $em;
        }


        /**
         * @return Nette\Application\UI\Form
         */
        protected function createComponentForm()
        {
                $form = new Nette\Application\UI\Form();

                $form->addHidden('city');

                $form->addText('name', 'Název pobočky')
                    ->setRequired('Vyplňte prosím název pobočky.');

                $form->addText('telephone', 'Pevná linka');

                $form->addText('mobile', 'Mobilní telefon');

                $form->addTextArea('address', 'Adresa');

                $form->addTextArea('openTime', 'Otevírací doba');

                for ($i = 1; $i <= 25; $i++) {
                        $form->addUpload('photo' . $i);
                }

                $form->addSubmit("send", 'Přidat pobočku');
                $form->onSuccess[] = $this->formSucceeded;

                return $form;
        }

        /**
         * @param Nette\Application\UI\Form $form
         */
        public function formSucceeded(Nette\Application\UI\Form $form)
        {
                $values = $form->getValues();
                $arrayValues = $form->getValues(TRUE);

                $photoDao = $this->em->getDao(Photo::getClassName());
                $officeDao = $this->em->getDao(Office::getClassName());
                $cityDao = $this->em->getDao(City::getClassName());

                if (isset($values->id))
                {
                        $office = $officeDao->find($values->id);
                        $office->city = $cityDao->find($values->city);
                        $office->name = $values->name;
                        $office->telephone = $values->telephone;
                        $office->mobile = $values->mobile;
                        $office->address = $values->address;
                        $office->openTime = $values->openTime;
                } else {
                        $office = new Office;
                        $office->city = $cityDao->find($values->city);
                        $office->name = $values->name;
                        $office->telephone = $values->telephone;
                        $office->mobile = $values->mobile;
                        $office->address = $values->address;
                        $office->openTime = $values->openTime;
                }

                /**
                 * Photos process
                 */
                $photos = array();
                for ($i = 1; $i <= 25; $i++) {
                        $name = $arrayValues["photo" . $i];

                        if ($name->isOk()) {
                                $image = Nette\Utils\Image::fromFile($arrayValues["photo" . $i]);
                                $path = __DIR__ . '/../../../www/static/img/pobocky/';

                                // photo name
                                $name = Nette\Utils\Random::generate(15) . '.jpg';

                                $image->save($path . '/' . $name, 80);

                                $photo = new Photo();
                                $photo->name = $name;
                                $photo->thumbnail = TRUE;

                                $image = Nette\Utils\Image::fromFile($arrayValues["photo" . $i]);
                                $image->resize(85, 85, Nette\Utils\Image::FILL | Nette\Utils\Image::EXACT);
                                $image->save($path . '/thumb_' . $name, 80);

                                $photoDao->save($photo);
                                $photos[] = $name;
                        }
                }

                $officeDao->save($office);

                // attach photos to office
                foreach ($photos as $photo) {
                        $office->addPhoto($photoDao->findOneBy(array("name" => $photo)));
                }

                $officeDao->save($office);
        }

}