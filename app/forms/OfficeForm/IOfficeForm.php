<?php

namespace App\Forms;

interface IOfficeForm
{
        /**
         * @return OfficeForm
         */
        function create();
}