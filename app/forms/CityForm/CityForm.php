<?php

namespace App\Forms;

use App\Entity\City;
use Kdyby;
use Nette;

/**
 * Class CityForm
 * @package App\Forms
 */
class CityForm extends BaseForm
{
        /** @var Kdyby\Doctrine\EntityManager */
        private $em;

        /**
         * @param Kdyby\Doctrine\EntityManager $em
         */
        public function __construct(Kdyby\Doctrine\EntityManager $em)
        {
                $this->em = $em;
        }


        /**
         * @return Nette\Application\UI\Form
         */
        protected function createComponentForm()
        {
                $form = new Nette\Application\UI\Form();

                $form->addText('name', 'Název města')
                    ->setRequired('Vyplňte prosím název města.');

                $form->addSubmit("send", 'Přidat město');
                $form->onSuccess[] = $this->formSucceeded;

                return $form;
        }

        /**
         * @param Nette\Application\UI\Form $form
         */
        public function formSucceeded(Nette\Application\UI\Form $form)
        {
                $values = $form->getValues();
                $cityDao = $this->em->getDao(City::getClassName());

                if (isset($values->id)) {
                        $city = $cityDao->findOneBy(array('id' => $values->id));
                        $city->name = $values->name;

                        $cityDao->save($city);
                } else {
                        $city = new City;
                        $city->name = $values->name;

                        if (!$cityDao->findBy(array('name' => $values->name))) {
                                $cityDao->save($city);
                        } else {
                                $form->addError('Toto město je již v databázi uloženo.');
                        }
                }
        }

}