<?php

namespace App\Forms;

interface ICityForm
{
        /**
         * @return CityForm
         */
        function create();
}