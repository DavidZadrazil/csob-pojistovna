<?php

namespace App\FrontModule\Presenters;

use App;
use Kdyby;
use Nette;

/**
 * Class BackendPresenter
 */
class BackendPresenter extends BasePresenter
{

        /** @var Kdyby\Doctrine\EntityManager @inject */
        public $em;

        /**
         * @return Nette\Application\UI\Form
         */
        protected function createComponentCreateEmployee()
        {
                $form = new Nette\Application\UI\Form();

                $form->addText('firstname', 'Jméno');
                $form->addText('lastname', 'Příjmení');
                $form->addText('position', 'Pozice');
                $form->addText('email', 'E-mail');
                $form->addText('mobile', 'Mobil');
                $form->addTextArea('profile', 'Profesní profil');
                $form->addTextArea('more', 'Více informací');
                $form->addTextArea('motto', 'Motto');
                $form->addTextArea('address', 'Adresa');
                $form->addUpload('photo', 'Fotografie');

                $form->addSubmit("send", "Přidat zaměstnance");
                $form->onSuccess[] = $this->createEmployeeSucceeded;

                return $form;
        }

        /**
         * @param Nette\Application\UI\Form $form
         */
        public function createEmployeeSucceeded(Nette\Application\UI\Form $form)
        {
                $values = $form->getValues();

                // employee dao
                $employees = $this->em->getDao(App\Entity\Employee::getClassName());

                if ($values->photo->isOk()) {
                        $image = Nette\Utils\Image::fromFile($values->photo);
                        $path = __DIR__ . '/../../www/static/img/photos/';

                        // photo name
                        $name = Nette\Utils\Strings::webalize($values->firstname) . '-' . Nette\Utils\Strings::webalize($values->lastname) . '.jpg';

                        // thumbnail
                        $mainPhoto = $image;
                        $mainPhoto->crop(0, 0, $image->width, $image->height - 1000);
                        $mainPhoto->resize(392, 369, Nette\Utils\Image::EXACT);
                        $mainPhoto->save($path . 'thumbnail/' . $name);

                        $image->save($path . 'original/' . $name);

                } else {
                        $form['photo']->addError('Fotografii se nepodařilo nahrát. (' . $values->photo->getError() . ')');
                }

                $employee = new App\Entity\Employee();
                $employee->firstname = $values->firstname;
                $employee->lastname = $values->lastname;
                $employee->position = $values->position;
                $employee->email = $values->email;
                $employee->mobile = $values->mobile;
                $employee->profile = $values->profile;
                $employee->more = $values->more;
                $employee->motto = $values->motto;
                $employee->address = $values->address;
                $employee->photo = $name;
                $employees->save($employee);

                $this->redirect('this');
        }

        /**
         * @return Nette\Application\UI\Form
         */
        protected function createComponentEditEmployee()
        {
                $form = new Nette\Application\UI\Form();

                $form->addText('firstname', 'Jméno');
                $form->addText('lastname', 'Příjmení');
                $form->addText('position', 'Pozice');
                $form->addText('email', 'E-mail');
                $form->addText('mobile', 'Mobil');
                $form->addTextArea('profile', 'Profesní profil');
                $form->addTextArea('more', 'Více informací');
                $form->addTextArea('motto', 'Motto');
                $form->addTextArea('address', 'Adresa');
                $form->addUpload('photo', 'Fotografie');

                $form->addSubmit("send", "Upravit");
                $form->onSuccess[] = $this->editEmployeeSucceeded;

                return $form;
        }

        /**
         * @param Nette\Application\UI\Form $form
         */
        public function editEmployeeSucceeded(Nette\Application\UI\Form $form)
        {

        }

        /**
         * Create slug for employees
         */
        public function actionCreateSlugs()
        {
                // employee dao
                $employees = $this->em->getDao(App\Entity\Employee::getClassName());

                foreach ($employees->findAll() as $employee) {
                        $employee->slug = Nette\Utils\Strings::webalize($employee->firstname . ' ' . $employee->lastname);
                        $employees->save($employee);
                }

                $this->terminate();
        }

        /**
         * Create thumbnails
         */
        public function actionCreateThumbnails()
        {
                foreach (Nette\Utils\Finder::findFiles('*.jpg')->in(__DIR__ . '/../../www/static/img/photos/original/new') as $photo)
                {
                        $image = Nette\Utils\Image::fromFile($photo->getPathname());
                        $image->resize(440, 675, Nette\Utils\Image::EXACT | Nette\Utils\Image::FILL);
                        $image->save($photo->getPathname(), 40);
                }

                $this->terminate();
        }

        /**
         * Render List
         */
        public function renderList()
        {
                $employees = $this->em->getDao(App\Entity\Employee::getClassName());

                $this->template->employees = $employees->findAll();
        }

}
