<?php

namespace App\FrontModule\Presenters;

use Nette;
use PersonPhoto;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{


        /** @var \Kdyby\Doctrine\EntityManager @inject */
        public $em;


        /**
         * @return PersonPhoto
         */
        protected function createComponentPerson()
        {
                return new PersonPhoto($this->em);
        }
}
