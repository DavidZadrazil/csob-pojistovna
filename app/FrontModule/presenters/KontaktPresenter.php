<?php

namespace App\FrontModule\Presenters;

use App\Entity\Office;
use Nette;
use PersonPhoto;


/**
 * Class KontaktPresenter
 */
class KontaktPresenter extends BasePresenter
{

        /** @var \Kdyby\Doctrine\EntityManager @inject */
        public $em;

        /**
         * Render Default
         */
        public function renderDefault()
        {
                $officesDao = $this->em->getDao(Office::getClassName());

                $prepared = array();
                foreach ($officesDao->findAll() as $office)
                {
                        $employees = array();
                        foreach ($office->employees as $employee)
                        {
                                $employees[$employee->id] = $employee;
                        }

                        $prepared[$office->id]['employees'] = $employees;
                }

                $this->template->offices = $prepared;
        }

        /**
         * @return PersonPhoto
         */
        protected function createComponentPerson()
        {
                return new PersonPhoto($this->em);
        }

}
