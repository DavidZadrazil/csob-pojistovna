<?php

namespace App\FrontModule\Presenters;
use App\Entity\Actuality;
use Kdyby\GeneratedProxy\__CG__\App\Entity\Position;

/**
 * Class ONasPresenter
 */
class ONasPresenter extends BasePresenter
{

        /** @var \Kdyby\Doctrine\EntityManager @inject */
        public $em;

        /**
         * Render Default
         */
        public function renderDefault()
        {
        	$positionsDao = $this->em->getDao(Position::getClassName());
        	$actualityDao = $this->em->getDao(Actuality::getClassName());
                $positions = $positionsDao->findAll();

                $prepared = array();
                foreach ($positions as $position)
                {
                        $employees = array();
                        foreach ($position->employees as $employee)
                        {
                                $employees[$employee->id] = $employee;
                        }

                        $prepared[$position->id]['employees'] = $employees;
                }

                $this->template->tree = $prepared;

                // actualities
                $this->template->actualities = $actualityDao->findBy(array(), array("id" => "DESC"), 3);
        }

}
