<?php

namespace App\FrontModule\Presenters;

use App;
use Nette;
use Kdyby;


/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{

        /**
         * @var Kdyby\Doctrine\EntityManager @inject;
         */
        public $em;

	public function renderDefault()
	{
		$employees = $this->em->getDao(App\Entity\Employee::getClassName());

                $selectedEmployees = $employees->findBy(array(), array('id' => 'ASC'));

                $checkedEmployees = array();
                foreach ($selectedEmployees as $employee)
                {
                        if ($employee->photo)
                        {
                                $checkedEmployees[] = $employee;
                        }
                }

                $random = range(1, count($checkedEmployees));
                shuffle($random);
                $random = array_slice($random, 0, 4);

                $choosed = array();
                foreach ($random as $person)
                {
                        // workaround for wtf bug from selection
                        if ($person == 47) $person = 25;
                        if ($person == 48) $person = 25;
                        if ($person == 49) $person = 25;

                        $choosed[] = $checkedEmployees[$person];
                }

                $this->template->choosed = $choosed;
	}

}
