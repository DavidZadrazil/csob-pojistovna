<?php

namespace App\FrontModule\Presenters;

/**
 * Class CizinciPresenter
 */
class CizinciPresenter extends BasePresenter
{
        /** @var \Kdyby\Doctrine\EntityManager @inject */
        public $em;

        /**
         * @return \Profile
         */
        protected function createComponentProfile()
        {
                return new \Profile($this->em);
        }

}
