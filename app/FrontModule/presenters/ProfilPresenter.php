<?php

namespace App\FrontModule\Presenters;

use App;
use Kdyby;

/**
 * Class ProfilPresenter
 */
class ProfilPresenter extends BasePresenter
{

        /** @var Kdyby\Doctrine\EntityManager @inject */
        public $em;

        /**
         * Render Default
         */
        public function renderDefault($name)
        {
                $employeeDao = $this->em->getDao(App\Entity\Employee::getClassName());

                // get employee
                $employee = $employeeDao->findOneBy(array('slug' => $name));
                if (!$employee) $this->error('Profil nebyl nalezen.');

                // address edit
                $address = $employee->address;
                $address = explode(',', $address);

                if (isset($address[2]))
                {
                        $address[1] = $address[2] . ', PSČ' . $address[1];
                        unset($address[2]);
                }

                $this->template->profile = $employee;
                $this->template->address = $address;
        }

}
