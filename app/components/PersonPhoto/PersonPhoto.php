<?php

use Kdyby\Doctrine\EntityManager;

class PersonPhoto extends Nette\Application\UI\Control
{

        private $em;

        /**
         * @param EntityManager $em
         */
        public function __construct(EntityManager $em)
        {
                $this->em = $em;
        }


        /**
         * @param $parameters
         */
        public function render($parameters)
        {
                $employees = $this->em->getDao(App\Entity\Employee::getClassName());

                // find employee
                $employee = $employees->find($parameters['id']);

                $templateName = (isset($parameters['type']) ? $parameters['type'].'.latte' : 'default.latte');

                $template = $this->template;
                $template->person = $employee;
                $template->setFile(__DIR__ . '/' . $templateName);
                $template->render();
        }

}