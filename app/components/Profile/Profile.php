<?php

use Kdyby\Doctrine\EntityManager;

class Profile extends Nette\Application\UI\Control
{

        private $em;

        /**
         * @param EntityManager $em
         */
        public function __construct(EntityManager $em)
        {
                $this->em = $em;
        }


        /**
         * @param $parameters
         */
        public function render($parameters)
        {
                $employees = $this->em->getDao(App\Entity\Employee::getClassName());

                // find employee
                $employee = $employees->find($parameters['id']);

                $template = $this->template;
                $template->setFile(__DIR__ . '/person-' . $employee->id . '.latte');
                $template->render();
        }

}